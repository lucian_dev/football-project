<?php

/**
 * Project form base class.
 *
 * @package    foootball
 * @subpackage form
 * @author     Crisan Lucian
 */
abstract class BaseFormPropel extends sfFormPropel
{
  public function setup()
  {
  }
}
