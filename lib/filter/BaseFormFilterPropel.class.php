<?php

/**
 * Project filter form base class.
 *
 * @package    foootball
 * @subpackage filter
 * @author     Crisan Lucian
 */
abstract class BaseFormFilterPropel extends sfFormFilterPropel
{
  public function setup()
  {
  }
}
