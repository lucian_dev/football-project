
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- team
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `team`;


CREATE TABLE `team`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(65)  NOT NULL,
	`players` INTEGER default 0,
	PRIMARY KEY (`id`),
	UNIQUE KEY `team_U_1` (`name`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- player
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `player`;


CREATE TABLE `player`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`team_id` INTEGER  NOT NULL,
	`first_name` VARCHAR(65),
	`last_name` VARCHAR(65),
	`number` INTEGER,
	`expires_at` DATETIME  NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `player_FI_1` (`team_id`),
	CONSTRAINT `player_FK_1`
		FOREIGN KEY (`team_id`)
		REFERENCES `team` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
