-- 
-- Table structure for table `daily_measurement`
-- 

CREATE TABLE `daily_measurement` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `sent_alert` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `daily_unique_key` (`meter_id`,`organization_id`,`measurement_date`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `fifteen_minute_measurement`
-- 

CREATE TABLE `fifteen_minute_measurement` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `import_flag` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `fifteen_minute_unique` (`meter_id`,`organization_id`,`measurement_date`,`measurement_time`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `hourly_measurement`
-- 

CREATE TABLE `hourly_measurement` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `hour_unique_key` (`meter_id`,`organization_id`,`measurement_date`,`measurement_time`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `monthly_measurement`
-- 

CREATE TABLE `monthly_measurement` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `monthly_unique_key` (`meter_id`,`organization_id`,`measurement_date`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `original_measurement`
-- 

CREATE TABLE `original_measurement` (
  `id` bigint(40) unsigned NOT NULL auto_increment,
  `measurement_updated_date` date NOT NULL,
  `measurement_updated_time` time NOT NULL,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `measurement_errorcode` varchar(50) NOT NULL,
  `import_flag` enum('Y','N') NOT NULL default 'N',
  `created_at` datetime NOT NULL,
  `actual_usage` float(20,10) default NULL,
  `registrator` varchar(25) default NULL,
  `manual_flag` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `org_wise_unique` (`meter_id`,`organization_id`,`measurement_date`,`measurement_time`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `weekly_measurement`
-- 

CREATE TABLE `weekly_measurement` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `week_unique_key` (`meter_id`,`organization_id`,`measurement_date`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `yearly_measurement`
-- 

CREATE TABLE `yearly_measurement` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `yearly_unique_key` (`meter_id`,`organization_id`,`measurement_date`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

-- 
-- Table structure for table `emission_summary`
-- 

CREATE TABLE `emission_summary` (
`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`meter_id` BIGINT NOT NULL ,
`organization_id` BIGINT NOT NULL ,
`emission_date` DATE NOT NULL ,
`emission_value` FLOAT( 20, 10 ) NOT NULL ,
`created_at` DATETIME NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 ALTER TABLE `emission_summary` 
ADD UNIQUE `meter_id` ( `meter_id` , `organization_id` , `emission_date` );


-- --------------------------------------------------------

-- 
-- Table structure for table `manual_summary`
-- 

CREATE TABLE `manual_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `meter_id` bigint(20) NOT NULL,
  `organization_id` bigint(20) NOT NULL,
  `measurement_value` float(20,10) NOT NULL,
  `measurement_date` date NOT NULL,
  `measurement_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `registrator` varchar(25) default NULL,
  PRIMARY KEY  (`id`),
  KEY `meter_id` (`meter_id`),
  KEY `organizaiton_id` (`organization_id`),
  KEY `measurement_value` (`measurement_value`),
  KEY `measurement_time` (`measurement_time`),
  KEY `measurement_date` (`measurement_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;