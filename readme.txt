Framework : Symfony 1.4
ORM: PROPEL
Javascript: Jquery


1. Import db structure.
	create a db with 'football'
	data->custom_db.sql

2. Config Data for project set DB name and access mysql:
	config->databases.yml
	dsn: 'mysql:host=localhost;dbname=football'    


3. LINKS: 
	/web/frontend_dev.php/player/index
	/web/frontend_dev.php/player/process

4. Module: Player
   Path:   apps\frontend\modules\player
   Controller where I wrote code :
      apps\frontend\modules\player\actions\actions.class.php

5. WorkFLow:
   	- go to link: localhost/[root_project]/web/frontend_dev.php/player/index and push YES link
    - at the Process PAGE you will see a list with 2 teams and players, and title
    		2 methods
    		TITLE : Retrieve Data  -> extract data from DB 
    		TITLE : Store Data  -> insert data from DB 