<?php

set_time_limit(0);
ini_set('memory_limit','-1');

/**
 * Player actions.
 *
 * @package    teamplayer
 * @subpackage players
 * @author     Lucian Crisan
 * @version    SVN: $Id: actions.class.php 12474 2014-30-03 10:41:27Z fabien $
 */

class playerActions extends sfActions {
    /**
     * This will be called before calling any actions
     *
     */
    public function preExecute () {

        $actionName = $this->getRequest()->getParameter('action');
        if($actionName == 'index' || $actionName == 'new' || $actionName == 'view' || $actionName == 'edit' ) {
            //set title
            $this->getResponse()->setTitle($this->getContext()->getI18N()->__('teamplayer'), false);
        }
    }

    /**
     * To Process Index action
     *
     * @param sfWebRequest $request
     */
    public function executeIndex (sfWebRequest $request) {
    }



    /**
     * Read form JSON and INSERT to DB
     *
     * @param sfRequest $request A request object
     */
    public function executeProcess(sfWebRequest $request) {

        $tmp_players = array();
        $tmp_teams = array();

        //check if exist data in DB
        if(!$this->checkTeams()) {
            $this->status = 'Store Data';

            $tmpArr = $this->dataStore();

            $tmp_players = $tmpArr[0];
            $tmp_teams = $tmpArr[1];

        } else {
            $this->status = 'Retrieve Data';

            $tmpArr = $this->dataRetrieve();

            $tmp_players = $tmpArr[0];
            $tmp_teams = $tmpArr[1];
        }

        //assign to the page
        $this->players = $tmp_players;
        $this->teams   = $tmp_teams;

        //unset
        unset($tmp_players);
        unset($tmp_teams);

        $this->setLayout("layout");
    }

    /**
     * Save team 
     * @param type array $option 
     * @return type int - new ID
     */
    private function saveTeam($option) {
        $team = new Team();
        $team->setName($option['name']);
        $team->save();

        return $team->getId();
    }


    /**
     * Save team 
     * @param type array $option 
     * @return type int - new ID
     */
    private function savePlayer($option) {
        $player = new Player();
        $player->setFirstName($option['first']);
        $player->setLastName($option['last']);
        $player->setNumber($option['number']);
        $player->setTeamId($option['teamNewId']);
        $player->save();

        return $player->getId();
    }


    /**
     * Update team 
     * @param type array $option 
     * @return type obj 
     */
    private function updateTeam($option) {
        $team = TeamQuery::create()->findPK($option['id']);
        $team->setPlayers($option['total']);
        $team->save();

        return $team;
    }


    /**
     * Check Data from DB if exist Germany team
     * @return return bool
     */
    private function checkTeams() {
       $data = TeamQuery::create()
                ->filterByName('Germany')
                ->find()->toArray();

        if(is_array($data) && count($data) >0 ) return true;
        return false;
    }

    /**
     * Parse array and create a single array without duplicates
     * @param type array $bigTeam 
     * @param type array $arrPlayers 
     * @return type array tmp_players
     */
    private function createTeam(array $bigTeam, $arrPlayers) {

        $tmp_players = $arrPlayers; $listPlayer = $bigTeam['players'];

        foreach ($listPlayer as $key => $player) {
            //check player if have ID
            if($player['id']){
                $tmp_players[$player['id']]['firstName'] = $player['firstName'];
                $tmp_players[$player['id']]['lastName']  = $player['lastName'];
                $tmp_players[$player['id']]['number']    = $player['number'];
                $tmp_players[$player['id']]['teamId']    = $bigTeam['meta']['teamId'];
            }
        }
        return $tmp_players;
    }

    /**
     * Insert in DB and format array for template
     * @return array $data
     */
    private function dataStore() {
        //set path for JSON files
        $team_1 = dirname(__FILE__).'/../../../../../data/json/Germany.json';
        $team_2 = dirname(__FILE__).'/../../../../../data/json/BayernMunich.json';

            //read files and convert to arrays
        $jsonTeam_1 = json_decode(file_get_contents($team_1), true);
        $jsonTeam_2 = json_decode(file_get_contents($team_2), true);


            //create a single array without duplicates
        $tmp_count = 0;
        $tmp_players = array();
        $tmp_teams = array();


            //for GERMANY TEAM
        $tmp_players = $this->createTeam($jsonTeam_1, $tmp_players);

        if($jsonTeam_1['meta']['teamId']) {
            //germany TEAM 
            $tmp_teams[$jsonTeam_1['meta']['teamId']] = $jsonTeam_1['meta'];
        }

            //for Bayern TEAM
        $tmp_players = $this->createTeam($jsonTeam_2, $tmp_players);

        if($jsonTeam_2['meta']['teamId']) {
            //germany TEAM 
            $tmp_teams[$jsonTeam_2['meta']['teamId']] = $jsonTeam_2['meta'];
        }

            //insert/update to db
        try {
            foreach ($tmp_teams as $key => $team) {
                $option['name'] = $team['teamName'];
                $tmp_teams[$key]['newId'] = $this->saveTeam($option);
                $tmp_teams[$key]['total'] = 0;
            }

            foreach ($tmp_players as $key => $player) {
                $option['first']  = $player['firstName'];
                $option['last']   = $player['lastName'];
                $option['number'] = $player['number'];
                    //get new id from team array
                $option['teamNewId'] = $tmp_teams[$player['teamId']]['newId'];
                    //set total number of players for same team
                $tmp_teams[$player['teamId']]['total'] +=  1;

                $tmp_players[$key]['newId'] = $this->savePlayer($option);
            }

                //upate team with total of player
            foreach ($tmp_teams as $key => $team) {
                $option['id'] = $team['newId'];
                $option['total'] = $team['total'];

                $this->updateTeam($option);
            }
        } catch (Exception $e) {
            throw new Exception($e);
        }

        $data = array($tmp_players, $tmp_teams);        
        return $data;
    }

    /**
     * Retrieve data from DB and format array for template
     * @return array $data
     */
    private function dataRetrieve() {

        $tmp_players = $tmp_teams = array();
        //get DATA from DB
        $players = PlayerQuery::create()
                        ->joinWith('Player.Team')
                        ->find();

        //create array
        foreach ($players as $key => $player) {

            $obj_id = $player->getId();
            $obj_teamId = $player->getTeam()->getId();

            $tmp_players[$obj_id]['firstName'] = $player->getFirstName();
            $tmp_players[$obj_id]['lastName'] = $player->getLastName();
            $tmp_players[$obj_id]['number'] = $player->getNumber();
            $tmp_players[$obj_id]['teamId'] = $obj_teamId;

            $tmp_teams[$obj_teamId]['teamId'] = $obj_teamId;
            $tmp_teams[$obj_teamId]['teamName'] = $player->getTeam()->getName();
            $tmp_teams[$obj_teamId]['newId'] = $obj_teamId;
        }

        $data = array($tmp_players, $tmp_teams);        
        return $data;
    }
}
