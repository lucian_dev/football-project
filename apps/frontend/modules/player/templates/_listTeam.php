<div class="team-item">
    <h2 class="title">
        <?php echo $team['teamName'];?>
    </h2>
    <ul class="nav-bar">
        <?php $count = 0 ; ?>
        <?php foreach($players as $key => $player): ?>
            <?php if($player['teamId'] == $team['teamId']): ?>
                <li class="elem clearfix team_<?php echo $team['teamId']?>">
                    <span class='team-fullname'><?php echo $player['firstName']. ' ' . $player['lastName']?></span>
                    <span class="team-number"><?php echo $player['number']?></span>
                    <a href="#" class="link">Edit</a>
                </li>
                <?php $count++; ?>
            <?php endif;?>
        <?php endforeach; ?>
    </ul>
    <h6 class="title total">
        Total: <?php echo $count;?>
    </h6>
</div>

<!-- >>> $('.team_156').length
27
>>> $('.team_357').length
18 -->