<div id="content" class="admin-section">
	<div id="shadow-wrapper" class="team-tab-box">
		<div class="content-left home">
			<?php if($sf_user->getFlash('successMessage')): ?>
				<div class="success-msg admin-success-msg"><?php echo $sf_user->getFlash('successMessage');?></div><br />
			<?php endif; ?>
			<?php if ($sf_user->hasFlash('error')): ?>
				<div class="error"><?php echo $sf_user->getFlash('error') ?></div><br />
			<?php endif; ?>

			<h2 class="">
				DO you want to parse attatched files:
			</h2>
			<ul class="nav-bar">
				<li class="elem">BayernMunich.json</li>
				<li class="elem">Germany.json</li>
			</ul>
			<div class="actions-box">
				<a href="#" class="link">No</a>
				<?php echo link_to('Yes', 'player/process', array('class'  => 'link'))?>
			</div>
		</div>
	</div>
</div>
