<div id="content" class="admin-section">
	<div id="shadow-wrapper" class="team-tab-box">
		<div class="content-left home">
		    <h1 class="title"><?php echo $status?></h1>
			<div class="list-team clearfix">
			<?php foreach ($teams as $key => $team):?>
				<?php include_partial('listTeam', array('team' => $team, 'players' => $players)) ?>
			<?php endforeach; ?>
			</div>
			<div class="actions-box">
				<?php echo link_to('Back', 'player/index', array('class'  => 'link'))?>
			</div>
		</div>
	</div>
</div>
